#! /bin/bash
CURRENT_DIR=`pwd`
##Install and start minikube
cd /tmp && wget https://github.com/kubernetes/minikube/releases/download/v0.28.0/minikube-linux-amd64 && \
chmod +x minikube-linux-amd64 && sudo mv minikube-linux-amd64 /usr/local/bin/minikube && \
minikube start --memory 7000 --cpus 3

## Install Helm and log_generator chart

cd /tmp && wget https://storage.googleapis.com/kubernetes-helm/helm-v2.9.1-linux-amd64.tar.gz && \
tar xvzf helm-v2.9.1-linux-amd64.tar.gz && sudo mv linux-amd64/helm /usr/local/bin/helm

helm init --upgrade && sleep 60 && cd ${CURRENT_DIR}/../helm && helm install log_generator
