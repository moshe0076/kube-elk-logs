#!/usr/bin/python

import datetime
import random
import socket
from time import sleep
from faker import Factory


fake = Factory.create()
hostname = socket.gethostname()
errorLevel = ['DEBUG', 'INFO', 'WARNING', 'ERROR']

while True:
    print(datetime.datetime.utcnow().isoformat()+" "+hostname+" "+random.choice(errorLevel)+" "+fake.sentence(nb_words=10))
    sleep(random.randint(1, 3))

